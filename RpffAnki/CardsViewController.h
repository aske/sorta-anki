//
//  CardsViewController.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dictionary.h"

@interface CardsViewController : UITableViewController
@property (nonatomic) Dictionary *dict;
@end
