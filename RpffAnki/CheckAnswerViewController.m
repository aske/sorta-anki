//
//  CheckAnswerViewController.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "CheckAnswerViewController.h"

@interface CheckAnswerViewController ()

@end

@implementation CheckAnswerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.TranslationLabel.text = self.answer;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleRightButton:(id)sender {
    self.TranslationLabel.text = @"Right";
    [self.delegate processGuess:self setIndex:self.currentIndex guessResult:YES];
    [self.navigationController popViewControllerAnimated:YES];    
}

- (IBAction)handleWrongButton:(id)sender {
    self.TranslationLabel.text = @"Wrong";
    [self.delegate processGuess:self setIndex:self.currentIndex guessResult:NO];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
