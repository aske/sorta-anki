//
//  NewCardViewController.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "NewCardViewController.h"
#import "CardsViewController.h"

@interface NewCardViewController ()

@end

@implementation NewCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleAddButton:(id)sender
{
    // No checks whatsoever, may add later
    NSDictionary *word = [NSDictionary dictionaryWithObject:self.translationEditor.text forKey:self.wordEditor.text];
    [self.delegate addItemViewController:self didFinishEnteringItem:word];
    [self.infoLabel setText:[NSString stringWithFormat:@"Added [%@ : %@]", self.wordEditor.text, self.translationEditor.text]];
    self.translationEditor.text = @"";
    self.wordEditor.text = @"";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
