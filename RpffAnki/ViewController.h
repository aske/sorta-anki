//
//  ViewController.h
//  RpffAnki
//
//  Created by aske on 31/5/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dictionary.h"
#import "NewCardViewController.h"

@interface ViewController : UIViewController <NewCardControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *ViewCardsButton;
@property (weak, nonatomic) IBOutlet UIButton *AddCardButton;
@property (nonatomic) Dictionary *dict;
@property (weak, nonatomic) IBOutlet UILabel *LearningSetSizeLabel;
@property (weak, nonatomic) IBOutlet UISlider *LearningSetSizeSlider;
@property (weak, nonatomic) IBOutlet UILabel *FinishPercentLabel;
@property (weak, nonatomic) IBOutlet UISlider *FinishPercentSlider;

@end
