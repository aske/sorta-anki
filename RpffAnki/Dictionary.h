//
//  Dictionary.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dictionary : NSObject
{
    NSMutableArray *_dictionary;
    NSString *_file;
}
- (id)initWithFile: (NSString*)filepath;
- (void)addNewEntry: (NSDictionary*)newCard;
- (NSArray*)getAllEntries;
- (NSArray*)getSeveralRandomEntries: (int)quantity;
- (int) count;

- (void)load;
- (void)save;

@property (nonatomic, retain) NSString *file;

@end
