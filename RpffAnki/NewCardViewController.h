//
//  NewCardViewController.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dictionary.h"

@class NewCardViewController;

@protocol NewCardControllerDelegate <NSObject>
- (void)addItemViewController:(NewCardViewController*)controller didFinishEnteringItem:(NSDictionary*)item;
@end

@interface NewCardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *wordEditor;
@property (weak, nonatomic) IBOutlet UITextField *translationEditor;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (nonatomic) Dictionary *dict;

@property (nonatomic, weak) id <NewCardControllerDelegate> delegate;

@end
