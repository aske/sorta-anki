//
//  LearningModeViewController.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "LearningModeViewController.h"
#import "CheckAnswerViewController.h"
#import "StatisticsTableViewController.h"
#import "Card.h"

@interface LearningModeViewController ()

@end

@implementation LearningModeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.EndButton.hidden = YES;
    self.EndButton.enabled = NO;
    self.stats = [[CardStatistics alloc] initWithSize:self.learningSet.count];
    self->_processingFirstCard = YES;
    self->_nextIndex = [self.stats nextIndex];
    Card *card = [self.learningSet objectAtIndex:self->_nextIndex];
    self.WordLabel.text = card.word;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)processGuess:(CheckAnswerViewController *)controller setIndex:(int)index guessResult:(BOOL)isCorrect
{
    [self.stats processGuess:index guessResult:isCorrect];
    
    if ([self.stats getRightPercentage] < self.finishPercent) {
        if (!self->_processingFirstCard) {
            self->_nextIndex = [self.stats nextIndex];
        }

        self.WordLabel.text = [[self.learningSet objectAtIndex:self->_nextIndex] word];
    } else {
        self.CheckAnswerButton.enabled = NO;
        self.EndButton.enabled = YES;
        self.EndButton.hidden = NO;
        self.WordLabel.text = [NSString stringWithFormat:@"Achieved %d%%", [self.stats getRightPercentage]];
    }
    NSLog(@"Right %% -> %d / %d", [self.stats getRightPercentage], self.finishPercent);
}

- (IBAction)handleCheckAnswer:(id)sender
{
    [self performSegueWithIdentifier:@"CheckAnswerSegue" sender:self];
}

- (IBAction)handleEndButton:(id)sender {
    [self performSegueWithIdentifier:@"FinishStatisticsSegue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CheckAnswerSegue"]) {
        CheckAnswerViewController *controller = (CheckAnswerViewController*)segue.destinationViewController;
        controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        controller.delegate = self;
        controller.currentIndex = self->_nextIndex;
        Card *card = [self.learningSet objectAtIndex:self->_nextIndex];
        
        controller.answer = [card translation];
        self->_processingFirstCard = NO;
    } else if ([segue.identifier isEqualToString:@"FinishStatisticsSegue"]) {
        StatisticsTableViewController *controller = (StatisticsTableViewController*)segue.destinationViewController;
        controller.stats = self.stats;
        controller.learningSet = self.learningSet;
    }
}

@end
