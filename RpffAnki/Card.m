//
//  Card.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "Card.h"

@implementation Card


- (id)init
{
    return [self initWithWord:@"" withTranslation:@""];
}

- (id)initWithWord: (NSString*)word withTranslation:(NSString*)translation
{
    if (self = [super init])
    {
        self->_data = [NSDictionary dictionaryWithObject:translation forKey:word];
    }
    return self;
}
- (NSString*)word
{
    return [[self->_data allKeys] firstObject];
}

- (NSString*)translation
{
    return [[self->_data allValues] firstObject];
}

- (NSDictionary*)toDictionary
{
    return self->_data;
}

- (Card*)fromDictionary: (NSDictionary*)dict
{
    self->_data = dict;
    return self;
}

@end
