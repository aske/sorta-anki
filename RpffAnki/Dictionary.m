//
//  Dictionary.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "Dictionary.h"

#include <stdlib.h>

@implementation Dictionary
@synthesize file = _file;

- (id)init
{
    return [self initWithFile:@"dict.plist"];
}

- (id)initWithFile: (NSString*)filepath
{
    if (self = [super init])
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docDir = [paths objectAtIndex: 0];
        NSString *actualFilepath = [docDir stringByAppendingPathComponent: filepath];
        _file = actualFilepath;
        _dictionary = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addNewEntry: (NSDictionary*)newWord
{
    [self->_dictionary addObject:newWord];
}

- (void)save
{
    id error;
    id plist = [NSPropertyListSerialization dataFromPropertyList:self->_dictionary format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    [plist writeToFile: self->_file atomically: NO];
}

- (void)load
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.file]) {
        self->_dictionary = [NSMutableArray arrayWithContentsOfFile:self.file];
    }
}

- (NSArray*)getAllEntries
{
    return (NSArray*)self->_dictionary;
}

- (NSArray*)getSeveralRandomEntries: (int)quantity
{
    NSMutableArray *sample = [self->_dictionary mutableCopy];
    
    NSUInteger count = [sample count];
    for (NSUInteger i = 0; i < count; ++i) {
        NSInteger nElements = count - i;
        NSInteger n = arc4random_uniform(nElements) + i;
        [sample exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    
    return [sample subarrayWithRange:NSMakeRange(0, quantity)];
}

- (int)count
{
    return self->_dictionary.count;
}
@end
