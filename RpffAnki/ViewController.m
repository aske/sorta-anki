//
//  ViewController.m
//  RpffAnki
//
//  Created by aske on 31/5/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "ViewController.h"
#import "CardsViewController.h"
#import "NewCardViewController.h"
#import "LearningModeViewController.h"
#import "Card.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.title = @"Main page";
    self.dict = [[Dictionary alloc] init];
    [self.dict load];
    [self.dict save];
    int value = ceil(self.LearningSetSizeSlider.value * self.dict.count);
    self.LearningSetSizeLabel.text = [NSString stringWithFormat:@"Set size: %d", value];
    self.FinishPercentLabel.text = [NSString stringWithFormat:@"%d%% to finish", (int)ceilf(self.FinishPercentSlider.value)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addItemViewController:(NewCardViewController *)controller didFinishEnteringItem:(NSDictionary *)item
{
    [self.dict addNewEntry:item];
    [self.dict save];
}

- (IBAction)handleViewCards:(id)sender {
    [self performSegueWithIdentifier:@"ViewCardsSegue" sender:self];
}
- (IBAction)handleAddCard:(id)sender {
    [self performSegueWithIdentifier:@"AddCardSegue" sender:self];
}
- (IBAction)handleStartLearningMode:(id)sender {
    if (self.dict.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Card database is empty.\n Add some cards."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        [self performSegueWithIdentifier:@"LearningModeSegue" sender:self];
    }
}
- (IBAction)handlerSetSizeSlider:(id)sender {
    int value = ceil(self.LearningSetSizeSlider.value * self.dict.count);
    self.LearningSetSizeLabel.text = [NSString stringWithFormat:@"Set size: %d", value];
}
- (IBAction)handleFinishPercentSlider:(id)sender {
    self.FinishPercentLabel.text = [NSString stringWithFormat:@"%d%% to finish", (int)ceilf(self.FinishPercentSlider.value)];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ViewCardsSegue"])
    {
        CardsViewController *controller = (CardsViewController*)segue.destinationViewController;
        controller.dict = self.dict;
    } else if ([segue.identifier isEqualToString:@"AddCardSegue"])
    {
        NewCardViewController *controller = (NewCardViewController*)segue.destinationViewController;
        controller.dict = self.dict;
        controller.delegate = self;
    } else if ([segue.identifier isEqualToString:@"LearningModeSegue"])
    {
        LearningModeViewController *controller = (LearningModeViewController*)segue.destinationViewController;
        int quantity = ceil(self.LearningSetSizeSlider.value * self.dict.count);
        NSArray* set = [self.dict getSeveralRandomEntries:quantity];
        NSMutableArray *cardSet = [set mutableCopy];
        for (int i = 0; i < cardSet.count; ++i) {
            NSDictionary* obj = [cardSet objectAtIndex:i];
            [cardSet replaceObjectAtIndex:i withObject:[[Card alloc] fromDictionary:obj]];
        }
        controller.learningSet = cardSet;
        controller.finishPercent = (int)ceilf(self.FinishPercentSlider.value);
    }
}

@end
