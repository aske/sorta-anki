//
//  StatisticsTableViewController.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardStatistics.h"

@interface StatisticsTableViewController : UITableViewController
@property (nonatomic) NSArray* learningSet;
@property (nonatomic) CardStatistics* stats;
@end
