//
//  CardStatistics.m
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import "CardStatistics.h"

@implementation CardStatistics
@synthesize wrongGuesses = _wrongGuesses;
@synthesize rightGuesses = _rightGuesses;

- (id)init
{
    NSLog(@"Card stat simple init");
    return [self initWithSize:0];
}

- (id)initWithSize: (int)size
{
    if (self = [super init])
    {
        self.indexedStatistics = [[NSMutableArray alloc]  initWithCapacity:size];
        for (int i = 0; i < size; ++i) {
            NSDictionary* stat = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:0], @"Right",
                                                                            [NSNumber numberWithInt:0], @"Wrong", nil];
            [self.indexedStatistics addObject:stat];
        }
        self->_rightGuesses = 0;
        self->_wrongGuesses = 0;
    }
    return self;
}

- (int)nextIndex
{
    // Disregards accumulated statistics as of now
    return arc4random_uniform(self.indexedStatistics.count);
}

- (void)processGuess:(int)index guessResult:(BOOL)correct
{
    correct ? self->_rightGuesses++ : self->_wrongGuesses++;
    NSMutableDictionary *newStat = [[self.indexedStatistics objectAtIndex:index] mutableCopy];
    NSString *correctness = correct ? @"Right" : @"Wrong";
    NSNumber *guesses = [newStat objectForKey:correctness];
    [newStat setObject:[NSNumber numberWithInt:([guesses intValue] + 1)] forKey:correctness];
    [self.indexedStatistics replaceObjectAtIndex:index withObject:newStat];
    NSLog(@"size: %d, from card stat +%d/-%d", [self size], self->_rightGuesses, self->_wrongGuesses);
}

- (int)getRightPercentage
{
    if ([self size] > (self->_rightGuesses + self->_wrongGuesses)) {
        return 0;
    } else {
        return (int)ceilf(100 * self->_rightGuesses / (self->_wrongGuesses + self->_rightGuesses));
    }
}

- (int)size
{
    return self.indexedStatistics.count;
}

- (int)rightGuessesAtIndex:(int)index
{
    NSDictionary* indexStat = [self.indexedStatistics objectAtIndex:index];
    NSNumber* guesses = [indexStat objectForKey:@"Right"];
    return [guesses intValue];
}

- (int)wrongGuessesAtIndex:(int)index
{
    NSDictionary* indexStat = [self.indexedStatistics objectAtIndex:index];
    NSNumber* guesses = [indexStat objectForKey:@"Wrong"];
    return [guesses intValue];
}

@end
