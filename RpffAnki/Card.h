//
//  Card.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
{
    NSDictionary *_data;
}
- (id)initWithWord: (NSString*)word withTranslation:(NSString*)translation;
- (NSString*)word;
- (NSString*)translation;
- (NSDictionary*)toDictionary;
- (Card*)fromDictionary: (NSDictionary*)dict;
@end
