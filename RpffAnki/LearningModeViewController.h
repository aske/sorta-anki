//
//  LearningModeViewController.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckAnswerViewController.h"
#import "CardStatistics.h"

@interface LearningModeViewController : UIViewController <CheckAnswerControllerDelegate>
{
    BOOL _processingFirstCard;
    int _nextIndex;
}

@property (weak, nonatomic) IBOutlet UIButton *CheckAnswerButton;
@property (weak, nonatomic) IBOutlet UILabel *WordLabel;
@property (weak, nonatomic) IBOutlet UIButton *EndButton;

@property (nonatomic) NSArray *learningSet;
@property (nonatomic) CardStatistics *stats;
@property (nonatomic) int finishPercent;
@end
