//
//  CardStatistics.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardStatistics : NSObject
{
    int _wrongGuesses;
    int _rightGuesses;
}
@property (nonatomic) NSMutableArray *indexedStatistics;
@property (nonatomic, readonly) int wrongGuesses;
@property (nonatomic, readonly) int rightGuesses;
- (id)initWithSize: (int)size;
- (int)nextIndex;
- (void)processGuess:(int)index guessResult:(BOOL)correct;
- (int)getRightPercentage;
- (int)size;
- (int)rightGuessesAtIndex:(int)index;
- (int)wrongGuessesAtIndex:(int)index;
@end
