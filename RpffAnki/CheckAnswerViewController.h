//
//  CheckAnswerViewController.h
//  RpffAnki
//
//  Created by aske on 1/6/14.
//  Copyright (c) 2014 seriousorganization. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CheckAnswerViewController;

@protocol CheckAnswerControllerDelegate <NSObject>
- (void)processGuess:(CheckAnswerViewController*)controller setIndex:(int)index guessResult:(BOOL)isCorrect;
@end

@interface CheckAnswerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *RightButton;
@property (weak, nonatomic) IBOutlet UIButton *WrongButton;
@property (weak, nonatomic) IBOutlet UILabel *TranslationLabel;

@property (nonatomic, weak) id <CheckAnswerControllerDelegate> delegate;

@property (nonatomic) int currentIndex;
@property (nonatomic) NSString *answer;

@end
